﻿namespace USRP_310
{
    public class UsrpBand
    {
        public int BandNumber { get; set; }

        public int StartFreqMhz { get; set; }

        public int EndFreqMhz { get; set; }

        public short GainCH1 { get; set; }

        public short GainCH2 { get; set; }

        public UsrpData Data { get; set; }

        public int CentralFreqMHz => (this.StartFreqMhz + this.EndFreqMhz) / 2;
    }
}