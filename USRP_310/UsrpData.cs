﻿namespace USRP_310
{
    using System.Collections.Generic;

    /// <summary>
    /// Class for storing parsed data from USRP.
    /// </summary>
    public class UsrpData
    {
        public UsrpData()
        {
            this.SpectrumCh1 = new List<short>(1024);
            this.SpectrumCh2 = new List<short>(1024);
            this.PhaseCh1 = new List<short>(1024);
            this.PhaseCh2 = new List<short>(1024);
        }

        public List<short> SpectrumCh1 { get; set; }

        public List<short> SpectrumCh2 { get; set; }

        public List<short> PhaseCh1 { get; set; }

        public List<short> PhaseCh2 { get; set; }
    }
}