﻿namespace USRP_310
{
    using System;
    using System.Linq;
    using System.Net;
    using System.Net.Sockets;
    using System.Threading;
    using System.Threading.Tasks;

    using NLog;

    /// <summary>
    /// Class used to work with USRP-E310.
    /// </summary>
    public class UsrpManager : IDisposable
    {

        private UdpClient client;

        private IPEndPoint localEndPoint;

        private IPEndPoint remoteEndPoint;

        public delegate Task UsrpDataDelegate(UsrpData responce);

        /// <summary>
        /// Event called when spectrum is received from the USRP.
        /// </summary>
        public event UsrpDataDelegate OnGetSpectrum;

        /// <summary>
        /// Gets a value indicating whether the execution of the read task.
        /// </summary>
        public bool IsListening { get; private set; }

        public ILogger Logger { get; set; } = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The dispose method.
        /// </summary>
        public void Dispose()
        {
            this.Logger.Trace("Start disposing.");
            this.client.Dispose();
        }

        /// <summary>
        /// Initiate UDP connection and start of receiving data from USRP device.
        /// </summary>
        /// <param name="localIp">Local IP address.</param>
        /// <param name="localPort">Local port.</param>
        /// <param name="remotePort">Remote port to send commands.</param>
        /// <param name="remoteIp">Remote address to send commands.</param>
        /// <param name="token"><see cref="CancellationToken"/> Token to cancel read task execution.</param>
        /// <returns>The <see cref="Task"/>.</returns>
        public Task Connect(
            string localIp,
            int localPort,
            int remotePort,
            string remoteIp,
            CancellationToken token = default)
        {
            var localEndPoint = new IPEndPoint(IPAddress.Parse(localIp), localPort);
            var remoteEndPoint = new IPEndPoint(IPAddress.Parse(remoteIp), remotePort);
            return Connect(localEndPoint, remoteEndPoint, token);
        }

        /// <summary>
        /// Initiate UDP connection and start of receiving data from USRP device.
        /// </summary>
        /// <param name="localEndPoint">
        /// The local end point.
        /// </param>
        /// <param name="remoteEndPoint">
        /// The remote end point.
        /// </param>
        /// <param name="token">
        /// <see cref="CancellationToken"/> Token to cancel read task execution.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task Connect(IPEndPoint localEndPoint, IPEndPoint remoteEndPoint, CancellationToken token = default)
        {
            this.localEndPoint = localEndPoint;
            this.remoteEndPoint = remoteEndPoint;
            try
            {
                this.client = new UdpClient(this.localEndPoint);
                this.Logger.Info(
                    string.Format(
                        "Start listen on {0}:{1}",
                        this.localEndPoint.Address,
                        this.localEndPoint.Port));
                _ = Task.Run(async () => await ReadSpectrumTask(token), token);
            }
            catch (Exception e)
            {
                this.Logger.Error(e, string.Format("Error while reading udp port data: {0}", e.Message));
            }
            finally
            {
                this.IsListening = false;
            }
        }

        /// <summary>
        /// Allows to set frequency and gain on both channels on USRP.
        /// </summary>
        /// <param name="frequencyMHz">
        /// Frequency value in MHz.
        /// </param>
        /// <param name="gainCH1">
        /// gain value from 5 to 70 for first channel
        /// </param>
        /// <param name="gainCH2">
        /// gain value from 5 to 70 for second channel
        /// </param>
        /// <param name="token">
        /// <see cref="CancellationToken"/> to cancel the operation
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task SetFrequency(int frequencyMHz, short gainCH1, short gainCH2, CancellationToken token = default)
        {
            int frequency1 = frequencyMHz << 8;
            int freqyency2 = frequencyMHz >> 8;
            int freqyency3 = (frequency1 | freqyency2) & 65535;

            if (gainCH1 < 5)
            {
                gainCH1 = 5;
            }

            if (gainCH1 > 70)
            {
                gainCH1 = 70;
            }

            int gainCH1_1 = gainCH1 << 8;
            int gainCH1_1_1 = gainCH1 >> 8;
            int gainCH1_1_1_1 = (gainCH1_1 | gainCH1_1_1) & 65535;

            int gainCH2_2 = gainCH2 << 8;
            int gainCH2_2_2 = gainCH2 >> 8;
            int gainCH2_2_2_2 = (gainCH2_2 | gainCH2_2_2) & 65535;

            var unum = (uint) freqyency3;

            // Convert to uint for correct >> with negative numbers
            var data = new[] {(byte) (unum >> 40), (byte) (unum)};
            var unum1 = (uint) gainCH1_1_1_1;

            // Convert to uint for correct >> with negative numbers
            var data1 = new[] {(byte) (unum1 >> 40), (byte) (unum1)};
            var unum2 = (uint) gainCH2_2_2_2;

            // Convert to uint for correct >> with negative numbers
            var data2 = new[] {(byte) (unum2 >> 40), (byte) (unum2)};

            byte[] newArray = new byte[data.Length + data1.Length];
            Array.Copy(data, 0, newArray, 0, data.Length);
            Array.Copy(data1, 0, newArray, data.Length, data1.Length);
            byte[] newArray1 = new byte[newArray.Length + data2.Length];
            Array.Copy(newArray, 0, newArray1, 0, newArray.Length);
            Array.Copy(data2, 0, newArray1, newArray.Length, data2.Length);

            this.Logger.Trace(string.Format("Sent set frequency command:[{0}]", string.Join(",", newArray1)));

            await this.client.SendAsync(newArray1, newArray1.Length, this.remoteEndPoint).ConfigureAwait(true);
        }

        /// <summary>
        /// Allows to set frequency and gain on both channels on USRP.
        /// </summary>
        /// <param name="band">
        /// <see cref="UsrpBand"/> band parameters to set.
        /// </param>
        /// <param name="token">
        /// <see cref="CancellationToken"/> to cancel the operation
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public Task SetFrequency(UsrpBand band, CancellationToken token = default)
        {
            return this.SetFrequency(band.CentralFreqMHz, band.GainCH1, band.GainCH2);
        }

        /// <summary>
        /// Extract several bits from integer number.
        /// </summary>
        /// <param name="number">
        /// The number from which bits are extracted.
        /// </param>
        /// <param name="count">
        /// The count of bits.
        /// </param>
        /// <param name="position">
        /// Start bit index.
        /// </param>
        /// <returns>
        /// Returns the number obtained from the extracted bits.
        /// </returns>
        private static int BitExtracted(int number, int count, int position)
        {
            return ((1 << count) - 1) & (number >> (position - 1));
        }

        /// <summary>
        /// The task of reading data from UDP port <see cref="localEndPoint"/>. 
        /// </summary>
        /// <param name="token">
        /// <see cref="CancellationToken"/> to cancel read task execution.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        private Task ReadSpectrumTask(CancellationToken token)
        {
            this.IsListening = true;
            while (!token.IsCancellationRequested)
            {
                try
                {
                    //todo to async receive.
                    var dataRead = this.client.Receive(ref this.localEndPoint);
                    if (dataRead.Length > 1000)
                    {
                        this.ParseData(dataRead);
                    }
                }
                catch (ObjectDisposedException ex)
                {
                    this.Logger?.Error(ex, "Client disposed. Stopped reading data task.");
                    break;
                }
                catch (Exception ex)
                {
                    this.Logger?.Error(ex, string.Format("Error while reading upd port!{0}", ex.Message));
                }
            }
            return Task.CompletedTask;
        }

        /// <summary>
        /// Parse data received from UDP port. Represent each 4 bytes as int number.
        /// In that number first 7 bits is amplitude, 9 bit is phase for 1ch.
        /// There is 1024 values in one UDP package.
        /// </summary>
        /// <param name="udpData">
        /// Data received from UDP port.
        /// </param>
        private void ParseData(byte[] udpData)
        {
            var data = new UsrpData();

            for (int i = 0; i < udpData.Length; i += 4)
            {
                byte[] temp = new byte[4];
                Array.Copy(udpData, i, temp, 0, temp.Length);
                int intValue = BitConverter.ToInt32(temp, 0);

                int phaseCh2 = BitExtracted(intValue, 9, 1);
                int phaseCh1 = BitExtracted(intValue, 9, 10);

                int spectrumCh2 = BitExtracted(intValue, 7, 19);
                int spectrumCh1 = BitExtracted(intValue, 7, 26);

                data.PhaseCh1.Add((short) phaseCh1);
                data.PhaseCh2.Add((short) phaseCh2);
                data.SpectrumCh1.Add((short) spectrumCh1);
                data.SpectrumCh2.Add((short) spectrumCh2);
            }

            this.Logger?.Trace(string.Format("Parsed upd data: {0} values.", data.PhaseCh1.Count));

            short[] swapList = new short[1024];
            Array.Copy(data.SpectrumCh1.ToArray(), data.SpectrumCh1.Count / 2, swapList, 0, data.SpectrumCh1.Count / 2);
            Array.Copy(data.SpectrumCh1.ToArray(), 0, swapList, data.SpectrumCh1.Count / 2, data.SpectrumCh1.Count / 2);

            data.SpectrumCh1 = swapList.ToList();

            Array.Copy(data.SpectrumCh2.ToArray(), data.SpectrumCh2.Count / 2, swapList, 0, data.SpectrumCh2.Count / 2);
            Array.Copy(data.SpectrumCh2.ToArray(), 0, swapList, data.SpectrumCh2.Count / 2, data.SpectrumCh2.Count / 2);
            data.SpectrumCh2 = swapList.ToList();

            this.OnGetSpectrum?.Invoke(data);
        }
    }
}