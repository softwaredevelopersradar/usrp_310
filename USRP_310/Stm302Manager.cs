﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace USRP_310
{
    using System.IO.Ports;
    using System.Text.RegularExpressions;
    using System.Threading;

    using NLog;

    public class Stm302Manager
    {
        public Stm302Manager()
        {
            this.Logger = LogManager.GetCurrentClassLogger();
        }

        public event EventHandler OnConnectPort;

        public event EventHandler OnDisconnectPort;

        public event EventHandler<float> OnFrequencyGet;

        public SerialPort Port { get; set; }

        public ILogger Logger { get; set; }

        public Task OpenPort(string portName, int baudRate)
        {
            if (this.Port == null)
            {
                this.Logger.Trace("Created new SerialPort object");
                this.Port = new SerialPort();
            }

            if (this.Port.IsOpen)
            {
                this.Logger.Trace("Port already opened. Close old one.");
                this.ClosePort();
            }

            try
            {
                this.Port.PortName = portName;
                this.Port.BaudRate = baudRate;
                this.Port.Parity = Parity.None;
                this.Port.DataBits = 8;
                this.Port.StopBits = StopBits.Two;
                this.Port.RtsEnable = true;
                this.Port.DtrEnable = true;
                this.Port.ReceivedBytesThreshold = 1000;
                this.Port.Open();

                this.Logger.Debug($"Port: {portName} opened!");


                this.OnConnectPort?.Invoke(this, EventArgs.Empty);
                return Task.CompletedTask;
            }
            catch (Exception ex)
            {
                this.Logger.Error($"Error during openning serial port {ex.Message}");
                this.OnDisconnectPort?.Invoke(this, EventArgs.Empty);
                return Task.FromException(ex);
            }
        }

        public Task ClosePort()
        {
            try
            {
                this.Port?.DiscardInBuffer();
                this.Port?.DiscardOutBuffer();
                this.Port?.BaseStream.Close();
                this.Port?.Close();
                this.Logger.Trace("Serial port closed");
                return Task.CompletedTask;
            }
            catch (Exception ex)
            {
                this.Logger.Error($"Error while closing com port {ex.Message}");
                return Task.FromException(ex);
            }
        }

        public async Task StartListen(CancellationToken token)
        {
            while (!token.IsCancellationRequested)
            {
                if (!this.Port.IsOpen)
                {
                    this.Logger.Error("Port is closed! Open port before start listen it!");
                }

                var readLine = this.Port?.ReadExisting();
                var code = "AT+FREQ=";
                if (readLine.Contains(code))
                {
                    var index = readLine.IndexOf(code, StringComparison.Ordinal);
                    float freq = -10;
                    try
                    {
                        var finalString = readLine.Substring(index + code.Length, 4);
                        freq = float.Parse(finalString);
                    }
                    catch (Exception e)
                    {
                        this.Logger?.Error(e, string.Format("Error in listen cycle:{0}", e.Message));
                    }

                    if (Math.Abs(freq - (-10)) > 0.01)
                    {
                        this.OnFrequencyGet?.Invoke(this, 4900 - freq);
                    }
                }

                await Task.Delay(200, token).ConfigureAwait(false);
            }
        }
    }
}
